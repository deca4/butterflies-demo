(* Butterflies demo
 * 2021-02-17 / deca3
 *)
open Sdl

let proc_events = function
  | Event.KeyDown { keycode = Keycode.Q }
  | Event.KeyDown { keycode = Keycode.Escape }
  | Event.Quit _ -> Sdl.quit (); exit 0
  | _ -> ()


let rec event_loop () =
  match Event.poll_event () with
  | None -> ()
  | Some ev ->
      proc_events ev;
      event_loop ()


let pixel_for_surface ~surface ~rgb =
  let fmt = Surface.get_pixelformat_t surface in
  let pixel_format = Pixel.alloc_format fmt in
  let pixel = Pixel.map_RGB pixel_format rgb in
  Pixel.free_format pixel_format;
  (pixel)


let load_sprite renderer ~filename =
  let surface = Surface.load_bmp ~filename in
  (* transparent pixel from white background *)
  let rgb = (255, 0, 255) in
  let key = pixel_for_surface ~surface ~rgb in
  Surface.set_color_key surface ~enable:true ~key;
  let tex = Texture.create_from_surface renderer surface in
  Surface.free surface;
  (tex)


let () =
  let width, height = (320, 240) in
  Sdl.init [`VIDEO];
  let window, render =
    Render.create_window_and_renderer ~width ~height ~flags:[]
  in
  let title = "Butterflies demo" in
  Window.set_title ~window ~title;
  Random.self_init ();

  let texfile = "./butterflies.bmp" in
  let texture = load_sprite render ~filename:texfile in

  let btfs = SExpr.parse_file "./butterflies.se" in
  let sprites =
    List.map (function
      SExpr.Expr
        [SExpr.Atom "sprite";
         SExpr.Expr [SExpr.Atom "name"; SExpr.Atom name];
         SExpr.Expr [SExpr.Atom "pos"; SExpr.Atom x; SExpr.Atom y];
         SExpr.Expr [SExpr.Atom "dim"; SExpr.Atom w; SExpr.Atom h]] ->
           let i = int_of_string in
           (name, ((i x), (i y), (i w), (i h)))
    | _ -> ("", (0, 0, 0, 0))
    ) btfs
  in
  let sprites = List.filter (function ("", _) -> false | _ -> true) sprites in
  let n_btf = List.length sprites in

  let hill = Rect.make4 0 160 width (height - 160) in
  let cloud = Rect.make4 30 20 60 40 in

  let draw_butterfly () =
    let _ = List.assoc "yellow" sprites in
    let btf = List.nth sprites (Random.int n_btf) in
    let (bname, (x, y, w, h)) = btf in

    let _x, _y = Random.int (width - (w*3)), Random.int (height - (h*3)) in

    let src_rect = Rect.make4 x y w h in
    let dst_rect = Rect.make4 _x _y (w*3) (h*3) in

    Render.copy render ~texture ~src_rect ~dst_rect ();
  in

  let render () =
    Render.set_draw_color render (10, 140, 240) 255;
    Render.clear render;
    Render.set_draw_color render (10, 180, 20) 255;
    Render.fill_rect render hill;
    Render.set_draw_color render (250, 250, 250) 255;
    Render.fill_rect render cloud;
    draw_butterfly ();
    Render.render_present render;
  in

  let rec main_loop () =
    event_loop ();
    render ();
    Timer.delay 3_000;
    main_loop ()
  in
  main_loop ()
