File:
 butterflies.png

From:
 https://opengameart.org/content/butterflies

Author: 
 Ivan Voirol

License(s): 
 CC-BY 3.0
 GPL 3.0
 GPL 2.0
 CC0
