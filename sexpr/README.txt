SExpr is a very simple parsing library for S-expressions.

It is released under MIT license.  
But yo can alse use it under Zlib license if you prefer.

Author: Florent Monnier

This version of the lib is able to parse strings between double quotes ""  
and end line comments starting with semicolons ;

If you don't need to parse strings (for spaces inclusion) and comments  
you can use ocaml-sexpr0 instead:  
https://github.com/fccm/ocaml-sexpr0
